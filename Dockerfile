# Use an official Node runtime as a parent image
FROM node:19

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package.json ./

# Install necessary Debian packages
RUN apt-get update && \
    apt-get install -y \
    libnss3 \
    libnss3-dev \
    libgdk-pixbuf2.0-dev \
    libgtk-3-dev \
    libasound2 \
    libxss-dev && \
    rm -rf /var/lib/apt/lists/*

# Install any needed packages
RUN npm install

# Bundle app source
COPY . .

# Run the app when the container launches
CMD ["npm", "run", "serve"]
