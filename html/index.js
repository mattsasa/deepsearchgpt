const chatHistory = document.getElementById("chatHistory")
const chatContainer = document.getElementById("chatContainer")
const input = document.getElementById("messageInput")
const sendButton = document.getElementById("sendButton")
const inProgress = document.getElementById("inProgress")

let reconnectInterval = null

const conversationHistory = []

const connectWebSocket = (isReconnecting) => {
    const socket = new WebSocket(`${window.location}`.replace("http", "ws"))

    socket.onmessage = (event) => {
        const message = JSON.parse(event.data)

        if (message.heartbeat) {
            if (inProgress.innerHTML == "") {
                inProgress.innerHTML = "<em>Please Wait </em>"
                chatContainer.scrollTop = chatContainer.scrollHeight
            } else {
                inProgress.innerHTML += ". "
            }
            return
        }

        // Regular message from the assistant
        inProgress.innerHTML = ""
        conversationHistory.push(message)
        addNewChatLine(message.role, message.content)
    }

    socket.onopen = () => {
        clearInterval(reconnectInterval)

        if (isReconnecting) {
            const warningMsg = "Reconnect successful."
            addNewChatLine("system", warningMsg)
            sendButton.disabled = false
        }

        socket.onclose = () => {
            addNewChatLine("warning", "You have been disconnected from the server. 😔")
            sendButton.disabled = true

            reconnectInterval = setInterval(() => {
                attemptReconnect()
            }, 1000)
        }
    }

    return socket
}

let socket = connectWebSocket(false)

const capitalize = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1)
}

const formatContent = (content) => {
    // Convert markdown to HTML
    const html = marked.parse(content)

    // Sanitize the HTML to prevent XSS attacks
    const cleanHtml = DOMPurify.sanitize(html)

    return cleanHtml
}


const addNewChatLine = (type, msg) => {
    let newHtml = `<p class="message ${type}"><strong>${capitalize(type)}</strong> ${formatContent(msg)}</p>`
    chatHistory.innerHTML += newHtml
    chatContainer.scrollTop = chatContainer.scrollHeight

    // Apply syntax highlighting
    document.querySelectorAll('pre code').forEach((block) => {
        hljs.highlightElement(block)
    })
}

const adjustChatHeight = () => {
    const inputContainerHeight = document.querySelector(".inputContainer").offsetHeight
    const titleHeight = document.getElementById("title").offsetHeight
    const otherElements = 170 + inputContainerHeight + titleHeight
    chatContainer.style.height = `calc(100vh - ${otherElements}px)`
    chatHistory.style.height = chatContainer.style.height - 1
}

const sendMessage = () => {
    if (input.value.trim() === "") return
    if (!socket || socket.readyState === WebSocket.CLOSED) return
    if (sendButton.disabled) return

    const message = { role: "user", content: input.value }

    socket.send(JSON.stringify(message))

    addNewChatLine("you", message.content)
    input.value = ""

    conversationHistory.push(message)
}

input.addEventListener("keydown", (e) => {
    if (e.key === "Enter" && !e.shiftKey) {
        // User hit enter key without the shift key
        e.preventDefault()
        sendMessage()
    }
})

window.onresize = adjustChatHeight
window.onload = () => {
    adjustChatHeight()
    addNewChatLine("assistant", "How can I assist you today?")
}

const attemptReconnect = () => {
    if (!socket || socket.readyState === WebSocket.CLOSED) {
        socket = connectWebSocket(true)
    }
}
