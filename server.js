import OpenAI from "openai"
import { createServer } from "http"
import { WebSocketServer } from "ws"
import express from "express"
import axios from "axios"
import puppeteer from "puppeteer"

const openai = new OpenAI()

const app = express()
const server = createServer(app)
const wss = new WebSocketServer({ server })


// Environment Params
const CX = process.env.MY_SEARCH_ENGINE_CX
const GOOGLE_API_KEY = process.env.MY_GOOGLE_API_KEY
const port = process.env.PORT

/// Constant Parameters
const TOKEN_LIMIT = 60000
const DONE_SEARCHING_TOKEN_THRESHOLD = 50000
const SINGLE_RESOURCE_MAX_TOKENS = 12000
const CHUNK_SIZE = 10

const SELECTED_LLM = "gpt-4o"

/// Model Instructions
const suggestQueriesInstructions = [
    "You are to process a user-submitted question that requires internet research for its answer. Your task is to recommend 2 to 5 search queries specifically for Google web search. These queries should be geared towards finding information relevant to the user's question.",
    "Your role is not to answer the question directly or conduct the searches. Your sole responsibility is to propose search queries.",
    "If the user's request involves writing, creating, or performing a specific task, imagine that you need to complete that task. List web search queries that you would use in the process of completing that task for gaining context, information, and research.",
    "If you believe the user's topic can be comprehensively researched with results from just two google searches, it is okay to ust return 2 queries. However, if you believe there is value in examining results from additional queries, please include those. Max of 5 queries.",
    `The search queries should be returned as a JSON string that can be parsed programatically. The structure of the JSON output should be as follows: { "search_queries": ["query1", "query2", ...]}.`,
    `If the user's message does not contain a question or a request, you may simply return an empty list of queries, like so: { "search_queries": []}`
].map((instruction) => {
    return { role: "system", content: instruction }
})

const pickResourcesInstructions = [
    "You are tasked with sorting a list of internet resources (URLs) based on their relevance to a user-submitted question.", 
    "Your input context will contain a user message with their question or request, followed by a list of Resources each containing the ID of that resource, the URL that you are choosing, and a brief snippet for added context.", 
    "Your sole responsibility is to order these resources so that the most relevant content appears first. Consider the following guidelines:",
    "Relevance to the Query: Prioritize URLs based on how closely their content aligns with the user's question. The most relevant resources should appear at the top of your sorted list.",
    "Focus on Direct Content Links: Favor URLs that lead directly to specific articles, stories, or detailed pages relevant to the query. Avoid links to general homepages or broad category pages. For example, choose 'https://www.example.com/article-about-topic' over 'https://www.example.com/'.",
    "Type of Content: Deprioritize URLs that lead to videos or PDFs. Place these at the end of your sorted list.",
    "Diversity and Uniqueness: Ensure the top resources are diverse and comprehensive. While multiple resources from the same domain are acceptable, they should link to unique and distinct content.",
    "Examine Each URL: Assess the relevance of each resource based on its URL's Domain name, path, params, and query. Prioritize URLs that appear to be a direct content link, where additional site navigation would not be necessary. This will inform your sorting decision.",
    `Return Format: Present your sorted list as a simple JSON array of resource IDs. For example: { "sorted_resource_ids": [5, 3, 4, 1, 6, 0, 2]}`,
    "Important: Ensure that the length of the output list is the same length as the input list. In other words, the number of IDs included your sorted list should be the same number of input resources, with no additions, omissions, or duplicates.",
    "Your role is to sort the URLs effectively, not to answer the user's question or access the resources. Use your judgment based on the available information to determine the relevance and order of the resources.",
    // "You are to process a user-submitted question followed by list of internet resources. Your task is to sort the resources such that the URLs of the first resources point to webpages that have initial content that is most relevant to the user's question or request.",
    // "Your role is not to answer the user's question or load the resouces. Your sole responsibility is to sort the resources based on relevancy of information that would be found in the initial content.",
    // "You should favor URLs that you think the initial content from the URL will contain the most and relevant information that would be needed to answer the user's question or request.",
    // "When sorting resources, you should examine each URL link such that they lead directly to specific articles, stories, or content pages. Avoid general homepages or category pages. For example, prefer 'https://www.example.com/article-about-topic' over 'https://www.example.com/'.",
    // "Use best judgement based on the information available to you to favor resources with URLs that you think would load detailed and individual content directly related to the user's question, such as a distinct news article, detailed blog post, or a specific research paper.",
    // "The focus should be on direct links to content that provides comprehensive information on its own, without the need to navigate further within the resource/website. For example a specific domain might have some great relevant content, but if the associated link just points to a homepage or table of contents page, then that would be a poor option that should be ranked low (appear towards the end in the sorted list).",
    // "Do not favor URLs that appear to lead to a video or a pdf, put those at the end of the list",
    // "The favored resoruces that you choose to be near the beginning of the sorted list that you return should be diverse and comprehensive, you should balance relevancy to the users question with information diversity and avoid duplication. It is okay to have multiple resources with URLs that are on the same domain, as long as they point to unique content.",
    // "You will receive the input in upcoming messages, where you will first get the user's quesiton or request. After that, you will get the list of internet resources, each containing a unique ID for that resource, a title for the resource, the URL, and a short snippet.",
    // "The sorted list that you create should be returned as a simple JSON array of all of the ids. The structure of the JSON output should be as follows: [ 5, 3, 4, 1, 6, 0, 2 ].",
    // "Your response should contain a list IDs only, no other information such as the titles, descriptions, or URLs should be included. The number of IDs in your response array should be equal to the number of IDs that are in the context.",
].map((instruction) => {
    return { role: "system", content: instruction }
})

const finalModelInstructions = [
    "You are an assistant tasked with thoroughly answering a user's question or request.",
    "You will receive a user-submitted question or request, followed by a list of resources. These resources provide additional context to help you more thoroughly answer the user's question.",
    "You will notice that the list of resources provided in the context will contain some URLs. Those are not meant to be accessed, but are included to provide context on the source of the information provided.",
    "Utilize both your training data and the provided context to formulate the best response.",
    "Be aware that the provided context may not always be accurate or factual. Cross-reference information, consider the reputation of sources, and use your judgment to assess accuracy.",
    "Provide thorough responses, citing information from the provided context or your prior knowledge when possible. Long, detailed answers are encouraged."
].map((instruction) => {
    return { role: "system", content: instruction }
})


/// Helper Functions
const hasValidResults = (result) => {
    return (
        result.status === "fulfilled" &&
        result.value &&
        result.value.data &&
        result.value.data.items &&
        result.value.data.items.length > 0
    )
}

const extractDesiredContent = (result) => {
    return result.value.data.items.map((item) => {
        return {
            title: item.title,
            link: item.link,
            snippet: item.snippet,
        }
    })
}

const otherFilters = (candidate) => {
    /// Future version could actually hit the URL to see if there is a PDF there.
    return !candidate.link.toLowerCase().includes("pdf") && !candidate.link.toLowerCase().includes("youtube.com")
}

const parseJsonArrayFromResponse = (response) => {
    const startIndex = response.indexOf("[")
    const endIndex = response.lastIndexOf("]")

    if (startIndex !== -1 && endIndex !== -1 && endIndex > startIndex) {
        const jsonString = response.slice(startIndex, endIndex + 1)

        try {
            // Attempt to parse the JSON string
            return JSON.parse(jsonString)
        } catch (error) {
            // If parsing fails, return the error
            console.log("Failed to parse JSON - See Below:")
            console.log(jsonString)
            return { error: "Failed to parse JSON" }
        }
    } else {
        // Return an error if valid JSON is not found
        return { error: "Valid JSON not found in response" }
    }
}


wss.on("connection", async (socket) => {
    // Set up a 20-second ping interval
    const connectionHeartbeat = setInterval(() => {
        socket.ping()
    }, 20000)

    socket.on("close", () => {
        clearInterval(connectionHeartbeat)
    })

    socket.on("message", async (msgString) => {
        let userMsg
        try {
            userMsg = JSON.parse(msgString)
        } catch (error) {
            console.log("Failed to parse JSON message")
            socket.send(JSON.stringify({ role: "warning", content: "Invalid Input" }))
            return
        }

        const inProgressHeartbeat = setInterval(() => {
            socket.send(JSON.stringify({ heartbeat: true }))
        }, 3000)

        try {

            /// Prepare Model Inputs
            const suggestQueriesInput = [...suggestQueriesInstructions]
            const pickResourcesInput = [...pickResourcesInstructions]
            const finalModelInput = [...finalModelInstructions]

            suggestQueriesInput.push({ role: "user", content: userMsg.content })
            pickResourcesInput.push({ role: "user", content: userMsg.content })

            // Step 1 - OpenAi Model suggests queries
            const completion1 = await openai.chat.completions.create({
                messages: [...suggestQueriesInstructions, userMsg],
                model: SELECTED_LLM,
                response_format: { type: "json_object" }
            })

            // console.log(completion1.choices[0].message.content)
            const search_queries = parseJsonArrayFromResponse(completion1.choices[0].message.content)

            if (search_queries.error) {
                console.log(search_queries.error)
                socket.send(JSON.stringify({ role: "warning", content: `Internal Server Error: ${search_queries.error}` }))
                return
            }

            if (search_queries.length == 0) {
                socket.send(JSON.stringify({ role: "warning", content: "Invalid Question" }))
                return
            }

            socket.send(JSON.stringify({ role: "assistant", content: search_queries }))

            // Step 2 - Use Google Custom Search JSON API
            const searchResults = await Promise.allSettled(
                search_queries.map(async (query) => {
                    const params = { key: GOOGLE_API_KEY, cx: CX, q: query }
                    return await axios.get("https://www.googleapis.com/customsearch/v1", { params })
                })
            )

            const resourceCandidates = searchResults
                .filter(hasValidResults)
                .map(extractDesiredContent)
                .flat()
                .filter(otherFilters)

            // Filter out duplicates
            const resourcesByLink = {}
            const resourcesById = {}
            let id = 0
            for (let candidate of resourceCandidates) {
                if (resourcesByLink[candidate.link] == undefined) {
                    resourcesByLink[candidate.link] = candidate
                    resourcesByLink[candidate.link].id = id
                    resourcesById[id] = candidate
                    id++
                }
            }

            const unsortedResources = Object.keys(resourcesByLink)

            // console.log(unsortedResources)

            socket.send(JSON.stringify({ role: "assistant", content: `Found ${unsortedResources.length} Resource Pages` }))

            // console.log(Object.keys(resourcesByLink))
            // console.log(Object.values(resourcesByLink))

            // Step 3 - OpenAi Model sorts search result resources
            socket.send(JSON.stringify({ role: "assistant", content: "Prioritizing Results for Relevancy" }))
            Object.values(resourcesByLink).forEach((data) => {
                pickResourcesInput.push({ role: "system", content: `Resource ID: ${data.id} \nURL: ${data.link} \nSnippet: ${data.snippet} \n\n` })
            })

            const completion2 = await openai.chat.completions.create({
                messages: pickResourcesInput,
                model: SELECTED_LLM,
                response_format: { type: "json_object" }
            })

            // console.log(completion2.choices[0].message.content)
            const secondChatResult = parseJsonArrayFromResponse(completion2.choices[0].message.content)

            if (secondChatResult.error) {
                console.log(secondChatResult.error)
                socket.send(JSON.stringify({ role: "warning", content: `Internal Server Error: ${secondChatResult.error}` }))
                return
            }
            
            const sortedResultsResponse = secondChatResult.map((id) => { return resourcesById[id].link })

            // console.log(sortedResultsResponse)
            socket.send(JSON.stringify({ role: "assistant", content: sortedResultsResponse }))
            socket.send(JSON.stringify({ role: "assistant", content: `Success Sorting ${sortedResultsResponse.length} Results` }))


            // Step 4 - Puppeteer extracts web resources contants and adds to final context
            socket.send(JSON.stringify({ role: "assistant", content: "Examining Results. This could take a few minutes" }))

            const browser = await puppeteer.launch({ headless: "new", args: ['--no-sandbox'] })
            
            let pagesAddedToContext = 0
            let contextCharacterCount = 0
            for (let i = 0; i < sortedResultsResponse.length; i += CHUNK_SIZE) {
                const chunk = sortedResultsResponse.slice(i, i + CHUNK_SIZE)

                const results = await Promise.allSettled(chunk.map(async (url) => {
                    return { url, textContent: await scrapePage(browser, url) }
                }))

                const scrapedResults = results.filter(result => result.status === 'fulfilled').map(data => data.value)

                for (let data of scrapedResults) {
                    const resourceMetaData = resourcesByLink[data.url]
                    const content = `Title: ${resourceMetaData.title} \nURL: ${resourceMetaData.link} \n\n${data.textContent}`.slice(0, SINGLE_RESOURCE_MAX_TOKENS * 4)
                    const estimatedTokenTotal = (contextCharacterCount + content.length) / 4
                    if (estimatedTokenTotal < TOKEN_LIMIT) {
                        /// Only add this resource if it will fit in the token limit
                        contextCharacterCount += content.length
                        pagesAddedToContext++
                        finalModelInput.push({ role: "system", content })
                    }
                }

                const estimatedTokenTotal = contextCharacterCount / 4
                if (estimatedTokenTotal > DONE_SEARCHING_TOKEN_THRESHOLD) {
                    break
                }
            }

            await browser.close()

            socket.send(JSON.stringify({ role: "assistant", content: `Successfully Researched ${pagesAddedToContext} Webpages` }))

            // Step 5 - OpenAi Model takes all the context and responds to the user's question or request
            finalModelInput.push({ role: "system", content: "Now that you have all of this context. Please answer the user's question in the following message." })
            finalModelInput.push({ role: "user", content: `User's Question: ${userMsg.content}` })

            socket.send(JSON.stringify({ role: "assistant", content: "Generating Final Response. This could take a few minutes." }))

            /// Final OpenAI Interaction
            const finalCompletion = await openai.chat.completions.create({
                messages: finalModelInput,
                model: SELECTED_LLM,
            })

            socket.send(JSON.stringify(finalCompletion.choices[0].message))

        } catch (error) {
            console.log(error)
            socket.send(JSON.stringify({ role: "warning", content: `Internal Server Error: ${error}` }))
        } finally {
            // Ensure that the interval is always cleared
            clearInterval(inProgressHeartbeat)
        }
    })
})

const scrapePage = async (browser, url) => {
    const page = await browser.newPage()

    // Navigate the page to a URL
    await page.goto(url, { waitUntil: "networkidle2" })

    // Set screen size
    await page.setViewport({ width: 1920, height: 1080 })

    // Scroll the page
    await page.evaluate(() => {
        window.scrollTo(0, document.body.scrollHeight)
    })

    // Wait for potentially lazy-loaded content
    await new Promise((resolve) => setTimeout(resolve, 1000))

    const textContent = await page.evaluate(() => {
        return document.body.innerText
    })

    await page.close()

    return textContent
}

app.use(express.static("html"))

server.listen(port, () => {
    console.log(`App listening on port ${port}`)
})
